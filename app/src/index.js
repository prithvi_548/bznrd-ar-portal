import React from 'react';
import ReactDOM from 'react-dom';
import Dashboard from './screens/dashboard/components/Dashboard';
import * as serviceWorker from './serviceWorker';

const target = document.getElementById('bznrd_arportal-root');
if (target) { ReactDOM.render(<Dashboard />, target); }

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
