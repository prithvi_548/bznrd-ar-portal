<?php 
defined( 'ABSPATH' ) or die( 'Direct script access disallowed.' );

add_action( 'init', function() {

  add_filter( 'script_loader_tag', function( $tag, $handle ) {
    if ( ! preg_match( '/^bznrd_arportal-/', $handle ) ) { return $tag; }
    return str_replace( ' src', ' async defer src', $tag );
  }, 10, 2 );

  add_action( 'wp_enqueue_scripts', function() {
	  $asset_manifest = json_decode( file_get_contents( BZNRD_ARPORTAL_ASSET_MANIFEST ), true )['files'];
			$url = get_site_url();
    if ( isset( $asset_manifest[ 'main.css' ] ) ) {

      wp_enqueue_style( 'bznrd_arportal', $url . $asset_manifest[ 'main.css' ] );
    }
    wp_enqueue_script( 'bznrd_arportal-runtime', $url . $asset_manifest[ 'runtime-main.js' ], array(), null, true );

    wp_enqueue_script( 'bznrd_arportal-main', $url . $asset_manifest[ 'main.js' ], array('bznrd_arportal-runtime'), null, true );

    foreach ( $asset_manifest as $key => $value ) {
      if ( preg_match( '@static/js/(.*)\.chunk\.js@', $key, $matches ) ) {
        if ( $matches && is_array( $matches ) && count( $matches ) === 2 ) {
          $name = "bznrd_arportal-" . preg_replace( '/[^A-Za-z0-9_]/', '-', $matches[1] );
          wp_enqueue_script( $name, $url . $value, array( 'bznrd_arportal-main' ), null, true );
        }
      }

      if ( preg_match( '@static/css/(.*)\.chunk\.css@', $key, $matches ) ) {
        if ( $matches && is_array( $matches ) && count( $matches ) == 2 ) {
          $name = "bznrd_arportal-" . preg_replace( '/[^A-Za-z0-9_]/', '-', $matches[1] );
          wp_enqueue_style( $name, $url . $value, array( 'bznrd_arportal' ), null );
        }
      }
    }

  });
});
