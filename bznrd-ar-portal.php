<?php
/**
 * Main Plugin File
 *
 * @link              http://buzznerdtrucks.com/
 * @since             1.0.0
 * @package           Buzznerd AR Portal
 *
 * @wordpress-plugin
 * Plugin Name:       Buzznerd AR Portal
 * Plugin URI:        http://buzznerdtrucks.com/
 * Description:       This plugin will connnect react front to the pages.
 * Version:           1.0
 * Author:            buzznerdtrucks.com
 * Author URI:        buzznerdtrucks.com
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       Buzznerd AR Portal
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
defined( 'ABSPATH' ) or die( 'Direct script access disallowed.' );


define( 'BZRND_ARPORTAL_WIDGET_PATH', plugin_dir_path( __FILE__ ) . '/app' );
define( 'BZNRD_ARPORTAL_ASSET_MANIFEST', BZRND_ARPORTAL_WIDGET_PATH . '/build/asset-manifest.json' );
define( 'BZNRD_ARPORTAL_INCLUDES', plugin_dir_path( __FILE__ ) . '/includes' );

require_once( BZNRD_ARPORTAL_INCLUDES . '/enqueue.php' );
require_once( BZNRD_ARPORTAL_INCLUDES . '/shortcode.php' );
